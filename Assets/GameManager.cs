﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public GameObject EnemyPrefab;
    private GameObject Player;
    private GameObject DetectedEnemy;

    [Header("Player Values")]
    public float PlayerHealth;

    public int WeaponDamage = 5;
    public int WaveLevel;
    public float LevelTime = 20f;
    public float EnemySpawningTime = 5f;
    public int TotalKilledZombie;

    public AudioSource WeaponShootSound;

    [Header("Spawn Lcoations For Enemies")]
    public List<Transform> EnemySpawnLocations = new List<Transform>();


    public Animator GunSlider;

    [Header("UI Information Area")]
    public Text HealthBar;
    public Text WaveInfo;
    public Text TotalKills;
    public Text StartingTimeText;
    public Image HealthBarImage;

    public bool PlayerDead = false;

    public GameObject EndGame_;

    public ParticleSystem MuzzleFire;
    //Const--------------------
    const float PLAYER_HEALTH = 100f;
    const int WAVE_FIRST = 1;
    const int NO_TOUCH = 0;
    const int NO_HEALTH = 0;
    const float NUMBER_ONE_SECOND = 1;
    const float PLAYER_LOWEST_HEALTH = 1;
    const float TIME_END = 0;
    const int MOUSE_LEFT_BUTTON = 0;
    const float SPAWNING_TIME = 10f;
    const float ENEMY_TIME = 5f;
    const float WAVE_TOTAL_SECONDS = 20f;
    const float HEALTH_BARRIER_LIMIT = 20f;
    readonly string PLAYER_TAG = "Player";
    readonly string HEALTHBAR_INFORMATION = " /100";
    readonly string WAVE_LEVEL = "Wave Level ";
    readonly string TOTAL_KILLS = "Total Kills";
    readonly string STARTING = "Starting in ";
    readonly string END_OF_THE_SENTENCES = "...";

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag(PLAYER_TAG);
        PlayerHealth = PLAYER_HEALTH;
        WaveLevel = WAVE_FIRST;
        MuzzleFire.Stop();
    }

    public GameObject Return_Player()
    {
        return Player;
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayerDead)
        {
            InformationArea();

            //If we arent detecting the same enemy gameobject then assign
            if (DetectedEnemy != GvrPointerInputModule.Pointer.CurrentRaycastResult.gameObject)
            {
                DetectedEnemy = GvrPointerInputModule.Pointer.CurrentRaycastResult.gameObject;
            }

            //reduce the level time so we can increase the wave level. This increase the difficulty
            ReduceTheLevelTime();

            HealthBarColorChange();
            WeaponFire();
        }
        KillPlayer();

    }
    public void InformationArea()
    {
        #region Information Text
        HealthBar.text = PlayerHealth + HEALTHBAR_INFORMATION;
        HealthBarImage.fillAmount = PlayerHealth / PLAYER_HEALTH;
        WaveInfo.text = WAVE_LEVEL + WaveLevel.ToString();
        TotalKills.text = TOTAL_KILLS + TotalKilledZombie;
        StartingTimeText.text = STARTING + EnemySpawningTime.ToString("0") + END_OF_THE_SENTENCES;

        EnemySpawningSystem();
        #endregion
    }
    public void ReduceTheLevelTime()
    {
        LevelTime -= NUMBER_ONE_SECOND * Time.deltaTime;

        if (LevelTime <= TIME_END)
        {
            WaveLevel++;
            EnemyManager.Instance.IncreaseDifficulty();
            LevelTime = WAVE_TOTAL_SECONDS - WaveLevel;
        }
    }

    public void EnemySpawningSystem()
    {
        EnemySpawningTime -= 1f * Time.deltaTime;

        //Enemy spawning system
        if (EnemySpawningTime <= TIME_END)
        {
            int locationSelected = Random.Range(0, EnemySpawnLocations.Count);

            GameObject enemy = Instantiate(EnemyPrefab);
            enemy.transform.localPosition = EnemySpawnLocations[locationSelected].localPosition;
            float enemyTime = (WaveLevel / SPAWNING_TIME);
            StartingTimeText.gameObject.SetActive(false);
            EnemySpawningTime = ENEMY_TIME - enemyTime;
        }
    }
    public void WeaponFire()
    {

#if UNITY_ANDROID && !UNITY_EDITOR
            if (Input.touchCount > NO_TOUCH)
            {                
                MuzzleFire.Play();
                GunSlider.SetBool("Triggered",true);
                AttackToEnemy(WeaponDamage);
                WeaponShootSound.Play();
            }
            else
            {               
                MuzzleFire.Stop();
                GunSlider.SetBool("Triggered",false);
            }
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(MOUSE_LEFT_BUTTON))
        {
            MuzzleFire.Play();
            GunSlider.SetBool("Triggered", true);
            AttackToEnemy(WeaponDamage);
            WeaponShootSound.Play();
        }
        if (Input.GetMouseButtonUp(MOUSE_LEFT_BUTTON
))
        {
            GunSlider.SetBool("Triggered", false);
            MuzzleFire.Stop();
        }
#endif
    }

    public void KillPlayer()
    {
        //Player Die
        if (PlayerHealth < PLAYER_LOWEST_HEALTH)
        {
            PlayerHealth = NO_HEALTH;
            PlayerDead = true;
            EndGame();
        }
    }
    public void DecreasePlayersHealth(int value)
    {
        PlayerHealth -= value;
    }
    public void HealthBarColorChange()
    {
        //Change the health color
        if (PlayerHealth < HEALTH_BARRIER_LIMIT)
        {
            HealthBarImage.color = Color.red;
        }
    }
    public void EndGame()
    {
        EndGame_.SetActive(true);
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void AttackToEnemy(int hitValue)
    {
        if (DetectedEnemy != null && DetectedEnemy.GetComponent<IndividualEnemy>())
        {
            DetectedEnemy.GetComponent<IndividualEnemy>().GetHitByPlayer(hitValue);
        }
        else
        {
            return;
        }
    }
}
