﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class IndividualEnemy : MonoBehaviour
{
    public int HealthValue;
    public int AttackPowerValue;
    public int WalkSpeed;
    public float AttackSpeed;
    private float AttackCoolDown;

    NavMeshAgent agent;

    EnemyManager.EnemyStates currentState = EnemyManager.EnemyStates.Walking;
    Animator anim;
    GameObject Player;

    bool PointAdded = true;
    public Image HealthRepresentation;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameManager.Instance.Return_Player();

        HealthValue = EnemyManager.Instance.Return_EnemyHealth();
        AttackPowerValue = EnemyManager.Instance.Return_EnemyAttackPower();
        WalkSpeed = EnemyManager.Instance.Return_EnemyWalkSpeed();
        AttackSpeed = EnemyManager.Instance.Return_EnemyAttackSpeed();
        AttackCoolDown = EnemyManager.Instance.Return_EnemyAttackCoolDown();

        AgentSettings();
       

        FaceTarget();
    }

    public void AgentSettings()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(Player.transform.localPosition);
        agent.updateRotation = true;
        agent.speed = WalkSpeed;

        anim = GetComponent<Animator>();
    }
    void FaceTarget()
    {
        //Turn to the player
        Vector3 direction = (Player.transform.localPosition - transform.localPosition).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        float speedTurn = 5f;
        transform.rotation = Quaternion.Slerp(transform.localRotation, lookRotation, Time.deltaTime * speedTurn);
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.Instance.PlayerDead)
        {
            //if enemy not dead and distance to player is close than stop and start attacking to the player
            if (currentState != EnemyManager.EnemyStates.Dead && agent.remainingDistance < 3)
            {
                agent.isStopped = true;
                currentState = EnemyManager.EnemyStates.Attacking;
                AttackCoolDown -= Time.deltaTime;

                if (AttackCoolDown <= 0)
                {
                    AttackCoolDown = 1 / AttackSpeed;
                    EnemyManager.Instance.AttackToPlayer(AttackPowerValue);
                }
            }
            //if health is 0 or smaller kill the enemy
            if (HealthValue <= 0)
            {
                currentState = EnemyManager.EnemyStates.Dead;
                Destroy(gameObject.GetComponent<NavMeshAgent>());
                Destroy(gameObject.GetComponent<BoxCollider>());
                StartCoroutine(DeathRemove());
            }
            //states for the enemy 
            switch (currentState)
            {
                case EnemyManager.EnemyStates.Walking:
                    anim.SetBool("isWalking", true);
                    break;
                case EnemyManager.EnemyStates.Attacking:
                    anim.SetBool("isWalking", false);
                    anim.SetBool("isAttacking", true);

                    break;
                case EnemyManager.EnemyStates.Dead:
                    anim.SetBool("isAttacking", false);
                    anim.SetBool("isDead", true);
                    Killed();
                    break;
                default:
                    break;
            }
        }
    }
    public void GetHitByPlayer(int value)
    {
        //Reduce the health and reduce the health bar value
        HealthValue -= value;
        HealthRepresentation.fillAmount = HealthValue / 10f;
    }

    public void Killed()
    {
        //if dead increase the total killed zombie number
        if (PointAdded)
        {
            PointAdded = false;
            GameManager.Instance.TotalKilledZombie++;
        }
    }
    IEnumerator DeathRemove()
    {
        yield return new WaitForSeconds(2);

        Destroy(gameObject);
    }
}
