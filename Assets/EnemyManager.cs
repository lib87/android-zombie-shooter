﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager Instance;

    [Header("Enemy Information")]
    public int EnemyHealth = 5;
    public int EnemyAttackPower = 1;
    public int EnemyWalkSpeed = 2;

    public float EnemyAttackSpeed = 1f;
    private float EnemyAttackCooldown = 0f;

    public enum EnemyStates
    {
        Walking,
        Attacking,
        Dead
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    #region Return the variables
    public int Return_EnemyHealth()
    {
        return EnemyHealth;
    }
    public int Return_EnemyAttackPower()
    {
        return EnemyAttackPower;
    }
    public int Return_EnemyWalkSpeed()
    {
        return EnemyWalkSpeed;
    }
    public float Return_EnemyAttackSpeed()
    {
        return EnemyAttackSpeed;
    }
    public float Return_EnemyAttackCoolDown()
    {
        return EnemyAttackCooldown;
    }
    #endregion

    public void AttackToPlayer(int attackPower)
    {
        GameManager.Instance.DecreasePlayersHealth(attackPower);
    }
 
    //increase the difficulty
    public void IncreaseDifficulty()
    {
        EnemyHealth++;
        EnemyAttackSpeed++;
        EnemyAttackPower++;
        EnemyWalkSpeed++;
    }
}
